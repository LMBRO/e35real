// E35REAL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
using std::vector;
using std::string;
using std::stoi;
using std::to_string;
using std::cout;
using std::endl;

vector<bool> getSieve() {
	vector<bool> sieve(1000000, true);
	sieve[0] = sieve[1] = false; // 0 and 1 are not prime
	unsigned int i = 2;
	while (i <= 1000) {
		if (sieve[i]) {
			for (unsigned int j = i * 2; j < sieve.size(); j += i) {
				sieve[j] = false;
			}
		}
		++i;
	}
	return sieve;
}
int main()
{
	unsigned int count = 0;
	vector<bool> checked(1000000);
	vector<bool> sieve = getSieve();
	for (unsigned int p = 2; p != sieve.size(); ++p) {
		if (sieve[p] && !checked[p]) {
			checked[p] = true;
			if (p < 10) {
				cout << p << endl;
				++count;
				continue;
			}
			bool good = true;
			vector<unsigned int> temp;
			temp.push_back(p);
			string ps = to_string(p);
			for (unsigned int i = 1; i != ps.size(); ++i) {
				string left = ps.substr(0, ps.length() - 1);
				string right = ps.substr(ps.length() - 1, 1);
				ps = right + left;
				if (sieve[stoi(ps)]) {
					if (!checked[stoi(ps)])
						temp.push_back(stoi(ps));
					checked[stoi(ps)] = true;
				}
				else
					good = false;
			}
			if (good) {
				for (unsigned int p : temp) {
					cout << p << endl;
				}
				count += temp.size();
			}
		}
	}
	cout << "n: " << count << endl;
    return 0;
}

